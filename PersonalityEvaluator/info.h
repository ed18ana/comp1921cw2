#ifndef PERSONALITYEVALUATOR_INFO_H
#define PERSONALITYEVALUATOR_INFO_H

#include <stdio.h>

int checkIfQuestion(char * response);

int loadInfo(FILE * file);

#endif
