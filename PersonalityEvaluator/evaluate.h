#ifndef PERSONALITYEVALUATOR_EVALUATE_H
#define PERSONALITYEVALUATOR_EVALUATE_H

#include <stdio.h>

int loadTestData(FILE * file);

int loadResponseData(FILE * file);

int evaluateResponse(char * response);

#endif
